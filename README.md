# Dog-Ceo-py

This is a python app made with pygame and is a example of dog.ceo a simple image of dogs. [dog.ceo](https://dog.ceo)

## Installation

Clone the repository using Git. See the statement below for help.

```bash
git clone https://gitlab.com/naveen521kk/dog-ceo.git
cd dog-ceo-py
python main-pygame.py
```

## Requirements

After downloading the repository install the requirements.
```bash
pip install -r requirements.txt
```
This statement would install the reqiuired libraries to reun the program.


## Contributing

Anyone who would like to contribute to the project is more than welcome.
Basically there's just a few steps to getting started:

1. Fork this repo
2. Make your changes and write a test for them
3. Add yourself to the AUTHORS file and submit a pull request!

Note: Before you make a pull request, please run `make check`. If your code
passes then you should be good to go! Requirements for running tests are in
`requirements-dev@<python-version>.txt`. You may also want to run `tox` to
ensure that nothing broke in other supported environments, e.g. Python 3.

## License
[Apache License 2.0](https://choosealicense.com/licenses/apache-2.0/)

## This can be reached at:
### Git urls:
[github.com](https://github.com/naveen521kk/dog-ceo-py)

[gitlab.com](https://gitlab.com/naveen521kk/dog-ceo)

### Email: 
[naveen@syrusdark.website](mailto:naveen@syrusdark.website)
### Website: 
[Syrusdark Blog](https://blog.syrusdark.website)